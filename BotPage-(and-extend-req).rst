==============================================
The Extensions: Req and page and site settings
===============================================

Background
----------
There was a base application developed over several years and these structures
are a result of extracting the practices developed in these apps and making them
generic.

HtmlPage object
---------------
The main interface to multiBottle is the 'HtmlPage' object. An application
instances a HtmlPage object to access request information and specify response
settings.

Mako templates
~~~~~~~~~~~~~~
MultiBottle provides four standard mako templates all derived from the single
'htmlpage'.
The goal is for pages to be able to be based on 'htmlpage' and to have html
based structure able to be overidden by either 'sitepage' or 'navpage'. In an
even more perfect world, css would be capable of providing these overrides.

ExtReq
------
The extended request was in the legacy systems, but this has now been hopefully
evolved to become scaffolding and needed only within the HtmlPage objects.

siteSettings.site
-----------------
The same web server code may be deployed on mulitple servers.  The
siteSettings.site object is designed to specify information which may vary
between individual deployments.

The multiBottle code imports objects from siteSettings.site. This either
allows for a folder
named 'siteSettings' with a file named 'site.py' or alternatively a
siteSettings.py python file with a 'site' object.

paths(<server>)
~~~~~~~~~~~~~~~~
Paths is defined as a function which takes a server url as a parameter, and
returns the paths to be used.  The paramter can be ignored if a server instance
is implementing a single server.  But it can be possible for the same code
to be used to instaniate multiple servers, and, for example, each instance open
different databases, or use different template pages.

The paths are:
- makePath
-
