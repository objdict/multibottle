#!/usr/bin/env python3.5
"""
this is an automator to run pytest.
possible setups:

"""
import sys
print("python",sys.version)
import pytest,os

cdir=os.listdir()
if not 'tests' in cdir:
    print('chdir')
    os.chdir("multiBottle")
#print(os.listdir('.'))

sys.path.append('.')
#print(sys.path)
testargs=[] #['pytest']
#if debuging
testargs+= ['--pdb','--maxfail=1']
#now the test_pages
#test= './tests/test_pages.py::TestMembPages'
#testargs.append(test)
try:
    pytest.main(testargs)
except BaseException as e:
    print('Pytest returned:',e)
