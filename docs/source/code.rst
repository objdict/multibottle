Code
==============

These modules are at the top application level.
viewfields module
-----------------

.. automodule:: multibottle.basicPage
    :members:
    :show-inheritance:

.. automodule:: multibottle.sitePage
    :members:
    :show-inheritance:

.. automodule:: multibottle.withNav
    :members:
    :show-inheritance:

.. automodule:: multibottle.fmPage
    :members:
    :show-inheritance:
