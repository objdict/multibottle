Site Map Requirements
=======================

A site map allows pages to display navigation and perhaps the entire site map.

When displaying a page it is logical to be able to identify where page is within
the sitemap so that child pages can be displayed as navigation choices, as well as
the parent page.


A Site Map With Bottle
-------------------------

The overall flow of a bottle based server is that python app for the server is loaded
and remains loaded the entire time the server is live.

Duing initialisation, Pages are registered with the url dispatcher so that
prior to the processing web requests the dispatch table has been built.

The goal is to have a global 'sitemap' object and as urls are added to the
dispatcher, they are also added to the sitemap.

The complexity is that urls have are based on pattern matching. Typically the
pattern involves a fixed prefix, and may consist of entirely fixed content.

The 'reBottle' multi app framework is based on each app in a multiapp enviroment
haveing a fixed 'basepath' which is the same within the app, and the structure
for urls is::

    <server>/<basepath>/<localpath>

Where <localpath>  is any path within the app.

On the Salect site, the news section for example has base path of
<server>/<basepath><localpath>

Where <basepath> = 'News'

The routes are::

    @route('/')  #show all
    @route('/<idx:int>')
    @route('/<edit>/<idx:int>')

None of these actually needs to be added to the the sitemap, although 'edit'
suggests possible sitemap entries, none are required at this time.

Looking at the Members sub app,

Routes are::

    @route('/<subpagenm>')
    @route('/<subpagenm>/<idx:int>')
    @post('/<subpagenm>/<idx:int>')

    subapp(''/store')

    @route('/')


The reality is a sitemap entry should be added for each <subpagenm>

If considering a site built from these two subapps, the structure would be::

    (root level):  <home>  News Members
    News:  no sub pages
    Members:  <cards> <locations> <devices> <Stores>

    Members/Stores/<n>: <locations>  <items> <menus>

Note that once subpages for Stores, require extracting the store number from
the url at the time the sub page is selected.


Sitemap Operations
-------------------

Operations:

- add : add an entry to the map
- root: display the root nodes
- children:  display all child nodes to a specified node(requires url)
- siblings: display all sibling nodes to a specified node(requires url)
- parent: display the parent node to a specified node.
- find: locate a node from url
- local: reduce url to localpath


Operations not yet considered are how to deal with pages from a database or CMS
(Content Managment System).

Building the Sitemap
--------------------
Examples:
Generally, the site map should be available to all pages so additions
News and Members are added as apps, these should be added to the site map.
Any time a route is added, this should also add an entry to the sitemap.
