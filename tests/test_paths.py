import importlib,re,sys,os
import bottle
from bottle import mako_template as template
from bottle import route, post,run, request

#import saltSqlaDB

#from siteSettings.site import paths,production

from multibottle.mbextend import HtmlPage, ExtReq, trail_filter, fileExt_filter


#from news.index import app as newsapp

app = bottle.app.push()
@route('/')  #show all
@route('/<idx:int>')
@route('/<edit>/<idx:int>')  #could also work on credentials - but having path allows view also
def news(idx=None,edit=None):
    return "newpage!"
newsapp=app

#from members.index import app as memberapp
memberapp=bottle.app.push()

app=bottle.app.push()
#app.router.add_filter('trail', trail_filter)
app.router.add_filter('fileExt',fileExt_filter)

app.mount("/News",newsapp)


app.mount("/Members",memberapp)


prodresp='<html><body>Invalid URL</body></html>'

@route('/<f:fileExt:,index.*>')
#@route('/index')
def home(f):
    return renderPage('index.html')

@route('/salt')
@route('/salt/')
@route('/salt/<path:path>')
def promo_pg(path='index.html'):
    """ promopage ... renders pages from a 'promo section (/salt) """
    return renderPage('salt/'+ path)


@route('/host')
def hello():
    if production:return prodresp
    host= re.sub(r'^.*://','',request.url)+'/'
    host = re.match(r'(.*?)[:/]',host).group(1)
    return "host: {}".format(host)

@route('/script/<any>')
def hello(any):
    if production:return prodresp
    #host= re.sub(r'^.*://','',request.url)+'/'
    #host = re.match(r'(.*?)[:/]',host).group(1)
    return "script: {}".format(request.script_name)

@post('/data')
@route('/data')
def hello():
    if production:return prodresp
    return "data: {}".format(request.body.getvalue().decode("utf-8") )

@post('/m/<func>')
@route('/m/<func>')
def mfuncs(func):
    """ this looks like was created simply to test creating allData
    """
    req=SaltReq()

    req.allData=req.getData.copy()
    req.allData.update(req.postData)
    funcmod=importlib.import_module('m.'+func)
    res=funcmod.main(req)
    return res #"func: {}".format(func)



class TestRoute:
    def test_render(self):
        pg=HtmlPage()
        r = pg.render('index','index.html')
        #r= renderPage('index.html')
        assert '<body>helloworld</body>' in ''.join(r.split())
        assert '<html>' in r
        assert '</html>' in r


    def test_find(self):
        router=app.router
        #r1 = router.find('/')
        #assert False

if __name__ == '__xmain__':
    try:
        port=int(sys.argv[1])
    except (IndexError,ValueError):
        port=8080
    #app=bottle.app()
    #app.router.add_filter('trail', trail_filter)
    run(app=app,host='0.0.0.0', port=port, debug=True)
